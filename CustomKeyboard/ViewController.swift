//
//  ViewController.swift
//  CustomKeyboard
//
//  Created by Admin on 03.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController, GenericKeyboardDelegate {
    
    @IBOutlet weak var calculatorTextField: UITextField!
    
    enum KeyboardButtons: Int {
        case NUM_0
        case NUM_1
        case NUM_2
        case NUM_3
        case NUM_4
        case NUM_5
        case NUM_6
        case NUM_7
        case NUM_8
        case NUM_9
        case NUM_000 = 10
        case DOT = 11
        case DONE = 12
        case CLEAR = 13
        case DIVIDE = 14
        case MULTIPLY = 15
        case DELETE = 16
        case SUBTRACT = 17
        case ADDICT = 18
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.groupTableViewBackground
        let keyboardView = GenericKeyboard(xibFileName: "CalculatorKeyboardView",
                                           keyboardHeight: 250)
        keyboardView.delegate = self
        self.calculatorTextField.inputView = keyboardView
        self.calculatorTextField.clearButtonMode = .whileEditing
        self.calculatorTextField.placeholder = "Tap here"
        self.calculatorTextField.backgroundColor = UIColor.groupTableViewBackground
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }

    func buttonWasTapped(tag: Int) {
        let tagEnum: KeyboardButtons = KeyboardButtons(rawValue: tag)!
        
        switch tagEnum {
            case .ADDICT:
                self.addOperation("+")
            case .SUBTRACT:
                self.addOperation("-")
            case .MULTIPLY:
                self.addOperation("×")
            case .DIVIDE:
                self.addOperation("÷")
            case .DONE:
                self.done()
            case .CLEAR:
                self.calculatorTextField.text = ""
            case .DELETE:
                self.deleteLast()
            case .DOT:
                self.addDot()
            case .NUM_0:
                self.addZero()
            case .NUM_000:
                self.trippleZero()
            default:
                self.addNumber(String(tag))
        }
    }
    
    
    // MARK: calculator operations
    
    func addOperation(_ operation: Character) {
        if self.calculatorTextField.text != "" &&
            !CalculatorEngine.isLastCharacterOperation(self.calculatorTextField.text!) {
            self.calculatorTextField.insertText(String(operation))
        } else if CalculatorEngine.isLastCharacterOperation(self.calculatorTextField.text!) {
            self.calculatorTextField.text = CalculatorEngine.replaceLastOperator(self.calculatorTextField.text!, op: operation)
        }
    }
    
    func addNumber(_ number: String) {
        if (self.calculatorTextField.text?.characters.count)! >= 2 {
            let endIndex = self.calculatorTextField.text?.index((self.calculatorTextField.text?.endIndex)!, offsetBy: -2)
            let lastTwo = self.calculatorTextField.text?.substring(to: endIndex!)
            if lastTwo == ".0" || self.calculatorTextField.text == "0" {
                self.calculatorTextField.text = CalculatorEngine.replaceLastOperator(self.calculatorTextField.text!, op: number.characters.first!)
            }
        }
        
        if self.calculatorTextField.text == "0" {
            self.calculatorTextField.text = CalculatorEngine.replaceLastOperator(self.calculatorTextField.text!, op: number.characters.first!)
        } else {
            self.calculatorTextField.insertText(number)
        }
    }

    func addDot() {
        if self.calculatorTextField.text != "" &&
            !CalculatorEngine.isLastCharacterOperation(self.calculatorTextField.text!) &&
            !CalculatorEngine.alreadyDotExist(self.calculatorTextField.text!) {
            self.calculatorTextField.insertText(".")
        } else if self.calculatorTextField.text == "" {
            self.calculatorTextField.insertText("0.")
        }
    }

    func trippleZero() {
        if self.calculatorTextField.text != "" &&
            !CalculatorEngine.isLastCharacterOperation(self.calculatorTextField.text!) &&
            !CalculatorEngine.alreadyDotExist(self.calculatorTextField.text!) {
            self.calculatorTextField.insertText(".000")
        } else if CalculatorEngine.alreadyDotExist(self.calculatorTextField.text!) {
            self.calculatorTextField.insertText("000")
        } else if self.calculatorTextField.text == "" {
            self.calculatorTextField.insertText("0")
        }
    }
    
    // fix it
    func addZero() {
        self.calculatorTextField.insertText("0")
        /*
        if self.calculatorTextField.text != "0" &&
            CalculatorEngine.alreadyDotExist(expression: self.calculatorTextField.text!) {
            self.calculatorTextField.insertText("0")
        } else if self.calculatorTextField.text == "" {
            self.calculatorTextField.insertText("0")
        } else if !CalculatorEngine.validateExpression(expression: self.calculatorTextField.text!) {
            self.calculatorTextField.insertText("0")
        } else if CalculatorEngine.isLastCharacterOperation(expression: self.calculatorTextField.text!) {
            self.calculatorTextField.insertText("0")
        }*/
    }
    
    func deleteLast() {
        self.calculatorTextField.deleteBackward()
    }
    
    func done() {
        if self.calculatorTextField.text == "" ||
            self.calculatorTextField.text == "0" {
            self.view.endEditing(true)
        }
        
        if self.calculatorTextField.text != "" &&
            !CalculatorEngine.isLastCharacterOperation(self.calculatorTextField.text!){
            let res = CalculatorEngine.solve(expression: self.calculatorTextField.text!)
            self.calculatorTextField.text = String(res)
        }
    }
}

