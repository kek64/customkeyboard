//
//  CalculatorEngine.swift
//  CustomKeyboard
//
//  Created by Admin on 05.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit

struct Stack<T> {
    let values: [T]
    
    init(_ v: [T] = [T]()) {
        self.values = v
    }
    
    func push(element: T) -> Stack< T > {
        let newVal = [element] + self.values
        return Stack< T >(newVal)
    }

    func pop() -> (T?, Stack<T>) {
        if self.values.isEmpty {
            return (nil, self)
        }
        
        let first = self.values.first
        let cnt = self.values.count
        let newValues = self.values[1..<cnt]
        return (first, Stack<T>(Array(newValues)))
    }
}

typealias BinaryOperation = (Double, Double) -> Double

struct CalculatorEngine {
    
    static let opa = [ "^" : (prec: 4, rAssoc: true),
                "×" : (prec: 3, rAssoc: false),
                "÷" : (prec: 3, rAssoc: false),
                "+" : (prec: 2, rAssoc: false),
                "-" : (prec: 2, rAssoc: false)]
    
    static func solve(expression: String) -> Double {
        let isValidInputData = validateExpression(expression)

        if isValidInputData {
            let parsedExpression = parseInfix(expression: expression)
            return parsedExpression.characters.split{ $0 == " " }.map { String($0) }.reduce(Stack< Double >() , evalRPN).pop().0!
        } else {
            return 0.0
        }
    }
    
    static private func evalRPN(args: Stack< Double >, token: String) -> Stack< Double > {
        var result: Stack< Double >
        
        switch(token) {
            case "+":
                result = applyBinaryOperation(args, operation: +)
            case "-":
                result = applyBinaryOperation(args, operation: -)
            case "×":
                result = applyBinaryOperation(args, operation: *)
            case "÷":
                result = applyBinaryOperation(args, operation: /)
            default:
                result = args.push(element: Double(token)!)
        }
        
        return result
    }
    
    static private func applyBinaryOperation(_ args: Stack< Double > , operation: BinaryOperation) -> Stack< Double > {
        let (v1, s1) = args.pop()
        let (v2, s2) = s1.pop()
        return s2.push(element: operation(v2!, v1!))
    }
    
    static private func parseInfix(expression: String) -> String {
        let convertedExpression = prepareString(expression: expression)
        let tokens = convertedExpression.characters.split { $0 == " " }.map( String.init )
        return rpnArray(tokens: tokens).joined(separator: " ")
    }
    
    static private func prepareString(expression: String) -> String {
        func closureFunction() -> (Character) -> (String) {
            var capturedValue: String = ""
            return {
                if  $0 == "+" ||
                    $0 == "-" ||
                    $0 == "×" ||
                    $0 == "÷" {
                    capturedValue.append(" \($0) ")
                } else {
                    capturedValue.append(String($0))
                }
                
                return capturedValue
            }
        }
        
        return expression.characters.map{ closureFunction()($0) }.joined()
    }
    
    static func validateExpression(_ expression: String) -> Bool {
        if expression == "" { return false }
        
        let containsAddOp = (expression.characters.contains("+"))
        let containsSubtractOp = expression.characters.contains("-")
        let containsDivideOp = expression.characters.contains("÷")
        let containsMultiplyOp = expression.characters.contains("×")
        
        return containsDivideOp || containsMultiplyOp
            || containsAddOp || containsSubtractOp
    }
    
    static func isLastCharacterOperation(_ expression: String) -> Bool {
        if expression == "" { return false }
        
        let isAddOp: Bool = expression.characters.last! == "+"
        let isSubOp: Bool = expression.characters.last! == "-"
        let isMultOp: Bool = expression.characters.last! == "×"
        let isDivOp: Bool = expression.characters.last! == "÷"
        
        return isAddOp || isSubOp || isMultOp || isDivOp
    }
    
    static func replaceLastOperator(_ expression: String, op: Character) -> String {
        if expression == "" { return "" }
        var replaced = String()
        let index = expression.characters.count - 1
        for (i, char) in expression.characters.enumerated() {
            replaced += String((i == index) ? op : char)
        }
        return replaced
    }
    
    static func alreadyDotExist(_ expression: String) -> Bool {
        if expression == "" { return false }

        var counter: Int = 0

        for char in expression.characters.reversed() {
            if char == "." {
                counter += 1
            }
                
            if char == "+" ||
               char == "-" ||
               char == "×" ||
               char == "÷" {
               return counter > 0
            }
        }
        
        return counter > 0
    }
    
    static func isOnlyOneNull(_ expression: String) -> Bool {
        if expression == "" { return true }
        
        var counter: Int = 0
        
        for char in expression.characters.reversed() {
            if char == "0" {
                counter += 1
            }
            
            if char == "." {
                return true
            }
            
            if char  == "+" ||
                char == "-" ||
                char == "×" ||
                char == "÷" {
                return counter < 0
            }
        }
        
        return counter < 0
    }
    
    static private func rpnArray(tokens: [String]) -> [String] {
        var rpn: [String] = []
        var stack: [String] = []
        
        for token in tokens {
            switch token {
                case "(":
                    stack += [token]
                case ")":
                    while !stack.isEmpty {
                        let operation = stack.removeLast()
                        if operation == "(" {
                            break;
                        } else {
                            rpn += [operation]
                        }
                    }
                default:
                    if let o1 = opa[token] {
                        for op in stack.reversed() {
                            if let o2 = opa[op] {
                                if !(o1.prec > o2.prec || (o1.prec == o2.prec && o1.rAssoc)) {
                                    rpn += [stack.removeLast()]
                                    continue
                                }
                            }
                            break
                        }
                        
                        stack += [token]
                    } else {
                        rpn += [token]
                }
            }
        }
        
        return rpn + stack.reversed()
    }
    
}
