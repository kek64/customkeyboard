//
//  GenericKeyboard.swift
//  CustomKeyboard
//
//  Created by Admin on 04.03.17.
//  Copyright © 2017 Yura Vash. All rights reserved.
//

import UIKit
import Foundation

protocol GenericKeyboardDelegate: class {
    func buttonWasTapped(tag: Int)
}

class GenericKeyboard: UIView {

    weak var delegate: GenericKeyboardDelegate?
    private var keyboardView: UIView?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(xibFileName: String, frame: CGRect) {
        super.init(frame: frame)
        loadFromXib(xibFileName: xibFileName)
    }
    
    init(xibFileName: String, keyboardHeight: Int) {
        super.init(frame: CGRect(x: 0, y:0, width: 0, height: keyboardHeight))
        loadFromXib(xibFileName: xibFileName)
    }
    
    func loadFromXib(xibFileName: String) {
        self.keyboardView = Bundle.main.loadNibNamed(xibFileName, owner: self, options: nil)?[0] as? UIView
        self.addSubview(self.keyboardView!)
        self.keyboardView!.frame = self.bounds
    }
    
    @IBAction func actionKeyTapped(sender: UIButton) {
        self.delegate?.buttonWasTapped(tag: sender.tag)
        UIView.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }) { (_) -> Void in
            sender.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }
    }
    
}
